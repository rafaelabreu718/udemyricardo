﻿using SmartSchoolApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartSchool.API.Data
{
    public interface IRepository
    {
        void add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        bool SaveChanges();
       
     
        //aluno
        public Aluno[] GetAllAlunos(bool includeProfessor = false);
        public Aluno[] GetAllAlunosByDisciplinaID(int disciplinaId, bool includeProfessor = false);
        public Aluno GetAlunosById(int alunoId, bool includeProfessor = false);
        //Professor
        public Professor[] GetAllProfessores(bool includeAlunos = false);
        public Professor[] GetAllProfessoresByDisciplinasId(int disciplinaId, bool includeAlunos = false);
        public Professor GetProfessorById(int professorId, bool includeProfessor = false);
    }
}
