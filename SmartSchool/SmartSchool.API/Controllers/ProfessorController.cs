﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartSchool.API.Data;
using SmartSchoolApi.Data;
using SmartSchoolApi.Models;

namespace SmartSchool.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessorController : ControllerBase
    {

        private readonly IRepository _repo;


        public ProfessorController(IRepository repo)
        {
            _repo = repo;


        }
        [HttpGet]
        public IActionResult Get()
        {
            var result = _repo.GetAllProfessores(true);
            return Ok(result);
        }
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var aluno = _repo.GetProfessorById(id, false);
            if (aluno == null) return BadRequest("o professor não foi encontrado");
            return Ok(aluno);

        }

        [HttpPost]
        public IActionResult Post(Professor professor)
        {
            _repo.add(professor);
            if (_repo.SaveChanges())
            {

                return Ok(professor);
            }
            return BadRequest("Professor Não cadastrado");

        }
        [HttpPut("{id}")]
        public IActionResult Put(int id, Professor professor)
        {

            var Prof = _repo.GetProfessorById(id, false);
            if (Prof == null) return BadRequest("o professor não foi encontrado");

            _repo.Update(professor);
            if (_repo.SaveChanges())

            {
                return Ok(professor);
            }
            return BadRequest("Professor não Atualizado");
        }
        [HttpPatch("{id}")]
        public IActionResult Patch(int id, Professor professor)
        {
            var Prof = _repo.GetProfessorById(id, false);
            if (Prof == null) return BadRequest("o professor não foi encontrado");
            _repo.Update(professor);
            if (_repo.SaveChanges())

            {
                return Ok(professor);
            }
            return BadRequest("Professor não Atualizado");
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var prof = _repo.GetProfessorById(id, false);
            if (prof == null) return BadRequest("o professor não foi encontrado");

            _repo.Update(prof);
            if (_repo.SaveChanges())

            {
                return Ok("professor deletado");
            }
            return BadRequest("Professor não deletado");
        }

    }

}
