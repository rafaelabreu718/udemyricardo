﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartSchool.API.Data;
using SmartSchoolApi.Data;
using SmartSchoolApi.Models;

namespace SmartSchool.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlunoController : ControllerBase
    {
        private readonly IRepository _repo;
       
       
        public AlunoController( IRepository repo)
        {
            _repo = repo;
            
           
        }

      
        // GET: api/<Aluno>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _repo.GetAllAlunos(true);
            return Ok(result);
        }

        // GET api/<Aluno>/5
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var aluno = _repo.GetAlunosById(id, false);
            if (aluno == null) return BadRequest("O aluno não foi encontrado");
            return Ok(aluno);
        }
   
        // POST api/<Aluno>
        [HttpPost]
        public IActionResult Post(Aluno aluno)

        {
            _repo.add(aluno);
           if  (_repo.SaveChanges())
            {

                return Ok(aluno);
            }
            return BadRequest("Aluno Não cadastrado");

        }

        // PUT api/<Aluno>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, Aluno aluno)

        {
            var alu = _repo.GetAlunosById(id);
            if (alu == null) return BadRequest("Aluno não encontrado.");
            
                _repo.Update(aluno);
                if (_repo.SaveChanges())
                {

                    return Ok(aluno);
                }
                return BadRequest("Aluno Não cadastrado");

        }
            // Patch api/<Aluno>/5
            [HttpPatch("{id}")]
        public IActionResult Patch(int id, Aluno aluno)

        {
            var alu = _repo.GetAlunosById(id);
            if (alu == null) return BadRequest("Aluno não encontrado.");

            _repo.Update(aluno);
            if (_repo.SaveChanges())
            {

                return Ok(aluno);
            }
            return BadRequest("Aluno Não atualizado");
        }

        // DELETE api/<Aluno>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var aluno = _repo.GetAlunosById(id);
            if (aluno==null) return BadRequest("Aluno não encontrado.");
            {
                _repo.Delete(aluno);
                if (_repo.SaveChanges())
                {

                    return Ok(aluno);
                }
                return BadRequest("Aluno Não cadastrado");

            }
        }
    }
}
